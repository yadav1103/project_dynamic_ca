Introduction:

….yet add content 


Aim: instead of using static public and private key or CA Certification, dynamically generation of public and private key or CA Certification whenever the connection is initiated,  
….

Challenge: on what criteria dynamically, generation of public and private key or CA Certification using some of device unique id such IMEI, Mac address


Unique device ids: yet to write 
hardware identifiers. Hardware identifiers such as SSAID (Android ID) and IMEI,MAC address.








Work progress and summary:
Week-1 & 2 complete summaries:

1.1	Basic introduction regarding CA and PKI concepts and template of CA

Public Key Cryptography
AKA asymmetric cryptography solves the problem of two entities communicating securely without ever exchanging a common key, by using two related keys, one private, one public.

Ciphered text with the public key can only be deciphered by the corresponding private key, and verifiable signatures with the public key can only be created with the private key.

But if the two entities do not know each other yet they a way to know for sure that a public key corresponds to the private key of the other identity.The usual solution to this problem is to use a PKI.

Public Key Infrastructure - PKI
A PKI is an arrangement that binds public keys to identities by means of a Certificate Authority (CA).
A CA is a centralized trusted third party whose public key is already known.
This way when A speaks to Bob, Bob shows Alice a signed message by Trent, who Alice knows and trusts, that says "this public key K belongs to Bob". That signed message is called a certificate, and it can contain other info. Alice can verify the signature using Trent's public key and can know speak confidently to Bob.
It is also common to have a chain of trust. Alice speaks to Bob, Trent does not know Bob but knows Carol who knows Bob, so Bob shows Alice a chain of certificates, one from Carol that says which key belongs to Bob and one from Trent who says which key belongs to Carol. Even without knowing Carol, Alice can verify the certificate from Trent, be sure of Carol's key, and if her trust in Trent is transitive then she can also trust Carol as to who Bob is.
Note:
There is an interesting solution for public authentication of public-key information is the web-of-trust scheme, which uses third party attestations of self-signed certificates.

X.509
.509 is a standard from the International Telecommunication Union for PKI.
Among other things, it defines the format for public key certificates.
Defined over these RFCs:
•	Version 1 - RFC1422
•	Version 2 - RFC2459
•	Version 3 - RFC5280
A X.509 v3 digital certificate has this structure:
•	Certificate
o	Version
o	Serial Number
o	Algorithm ID
o	Issuer
o	Validity
	Not Before
	Not After
o	Subject
o	Subject public key info
o	Issuer Unique Identifier (optional)
o	Subject Unique Identifier (optional)
o	Extensions (optional)
	...
•	Certificate Signature Algorithm
•	Certificate Signature
Version, Serial, Algorithm ID and Validity
•	Version - Indicates X.509 version. Should be 3 (value 0x2).
•	Serial - Unique positive integer assigned by the CA to each certificate.
•	Algorithm ID - Must be the same as the field "Certificate Signature Algorithm"
•	Validity - Two dates that form the period when the certificate is valid.

Issuer and Subject
Each a Distinguished Name (DN), unique per CA.
A DN, described in RFC1779, consists of a single line with these separated values:
•	CN - CommonName
•	L - LocalityName
•	ST - StateOrProvinceName
•	O - OrganizationName
•	OU - OrganizationalUnitName
•	C - CountryName
Example:
C=PT, ST=Lisboa, L=Lisboa, O=Foo Org, OU=Bar Sector, CN=foo.org/emailAddress=admin@foo.org
The signing CA may not require all values.
When connecting to an HTTPS server, browsers will check the CN value and it should be conforming to the domain. Wildcard certificates usually start with a * in CN to allow any subdomain. e.g. CN=*.example.com
Note that browsers will reject the wilcard for the naked domain, i.e. example.com is not conforming to *.example.com.
However, a certificate can be used for an HTTPS server that replies in multiple different domains. Additional domains can be specified in the extension Subject Alternative Names.
Subject public key info
Contains the public key algorithm and its specific parameters. e.g.:
•	algorithm: rsa encryption
•	key size: 2048
•	exponent: 0x10001
•	modulus: 00:ec:82:3f:78:b6...
Issuer and Subject Unique Identifiers
Introduced in version 2 to permit the reuse of issuer and subject names. For example, suppose a CA goes bankrupt and its name is deleted from the country's public list, after some time another CA with the same name may register itself even though it is unrelated to the first one.
IMO, this is all very silly. Unsurprisingly, IETF recommends that no issuer and subject names be reused.
Extensions
Introduced in version 3. A CA can use extensions to issue a certificate only for a specific purpose, e.g. only for http servers.
Extensions can be critical or non-critical. Non-critical can be ignored, while critical must be enforced and the whole certificate must be rejected if the system does not recognize a critical extension.
Some standard extensions:
•	Subject Key Identifier
•	Authority Key Identifier
•	Subject Alternative Name
•	Basic Constraints
Authority and Subject Key Identifiers
Used where an entity has multiple signing keys. Identity can be verified by either name and serial number or by this key identifier.
An identifier is the 160-bit SHA-1 hash of the public key, or just the first 60 bits preceded with the bits 0100.
Subject Alternative Name
May contain additional DNS names or IP addresses where the certificate is valid, that is, besides the one specified in CN.
Basic Constraints
Whether the subject is a CA and optionally the maximum length of depth of certification paths.

Let's suppose we need a signed certificate for an HTTPS server. This means we need a certificate for the domain (or domains) where the server will be available. We need a certificate that the browser can verify and tell the user that he is on the right servers of the domain of the URL he typed and that a safe connection is established. Browsers use a certificate store which has a list of CAs. To check your you can go to your browser's settings, search for the Certificates section, maybe in Security or Advanced, there should be certificate manager. What we need is a certificate that says our key belongs to our domain issued (signed) by one of these entities. Or we can also have it issued by an intermediary entity, one who was authorized by one of the CAs to issue certificates.
Wildcard certificates
A wildcard certificate is a certificate which can be used with multiple subdomains of a domain.
Browsers look for the CN (Common Name) in the subject field which should be a domain, or a wildcard like *.example.com.Browsers will accept a certificate with CN *.example.org  for www.example.org, , login.example.org or or bo.example.org. But the "naked" domain example.org will not work. Additional domains (including the naked domain) may be added in the extension "SubjectAltName".



1.2	RSA-key pair generationOpenSSL

OpenSSL is a cryptography toolkit. Contains many subcommands, each with a man page of its own e.g. ca(1), req(1) , x509(1).
Most of OpenSSL's tools deal with -in and -out parameters. Usually you can also inspect files by specifying -in <file> and -noout, you also specify which part of the contents you're interested in, to see all use -text. Examples below.

Generate Keys and Certificate Signing Request (CSR)
generate an RSA key for the CA
:
openssl genrsa -out testing_1.org.key 2048

 

openssl genrsa is the tool to generate rsa keys. 2048 is the key size. This created a file testing.org.key that contains the private key.

Key inspection:
openssl rsa -in testing_1.org.key -noout -text


 
 

Any copy of the private key should only be help by the entity who is going to be certified. This means the key should never be sent to anyone else, including the certificate issuer.
We now generate a Certificate Signing Request which contains some of the info that we want to be included in the certificate. To prove ownership of the private key, the CSR is signed with the subject's private key.
Generate a CSR:
openssl req -new -key testing_1.org.key -out testing_1.org.csr
 

We can then take a look at the CSR's contents:
openssl req -in testing_1.org.csr -noout -text
 


The certificate is then sent to the issuer, and if he approves the request a certificate should be sent back.
your Signature Algorithm is not MD5. Old OpenSSL configurations have default_md = md5 as default. Browsers reject certificates that use md5 as a signature algorithm because it has been found to be insecure.
there are no extensions, to add extensions an additional config file is needed. This makes the process a bit more complicated so when you buy a wildcard certificate you don't usually need to specify the extension SubjectAltName for the naked domain because the issuer will do it for you.
# The main section is named req because the command we are using is req
# (openssl req ...)
[ req ]
# This specifies the default key size in bits. If not specified then 512 is
# used. It is used if the -new option is used. It can be overridden by using
# the -newkey option. 
default_bits = 2048

# This is the default filename to write a private key to. If not specified the
# key is written to standard output. This can be overridden by the -keyout
# option.
default_keyfile = oats.key

# If this is set to no then if a private key is generated it is not encrypted.
# This is equivalent to the -nodes command line option. For compatibility
# encrypt_rsa_key is an equivalent option. 
encrypt_key = no

# This option specifies the digest algorithm to use. Possible values include
# md5 sha1 mdc2. If not present then MD5 is used. This option can be overridden
# on the command line.
default_md = sha1

# if set to the value no this disables prompting of certificate fields and just
# takes values from the config file directly. It also changes the expected
# format of the distinguished_name and attributes sections.
prompt = no

# if set to the value yes then field values to be interpreted as UTF8 strings,
# by default they are interpreted as ASCII. This means that the field values,
# whether prompted from a terminal or obtained from a configuration file, must
# be valid UTF8 strings.
utf8 = yes

# This specifies the section containing the distinguished name fields to
# prompt for when generating a certificate or certificate request.
distinguished_name = my_req_distinguished_name


# this specifies the configuration file section containing a list of extensions
# to add to the certificate request. It can be overridden by the -reqexts
# command line switch. See the x509v3_config(5) manual page for details of the
# extension section format.
req_extensions = my_extensions

[ my_req_distinguished_name ]
C = PT
ST = Lisboa
L = Lisboa
O  = Oats In The Water
CN = *.oats.org

[ my_extensions ]
basicConstraints=CA:FALSE
subjectAltName=@my_subject_alt_names
subjectKeyIdentifier = hash

[ my_subject_alt_names ]
DNS.1 = *.oats.org
DNS.2 = *.oats.net
DNS.3 = *.oats.in
DNS.4 = oats.org
DNS.5 = oats.net
DNS.6 = oats.i


Notice the various DNS names. Since the configuration parser does not allow multiple values for the same name we use the @my_subject_alt_names and DNS.# with different numbers. With this configuration we can create a CSR with the proper extensions:

1.3	CA Key and self-signed Certificate:


openssl genrsa -out testing_1.key 2048
 

OpenSSL uses the information you specify to compile a X.509 certificate using the information prompted to the user, the public key that is extracted from the specified private key which is also used to generate the signature.
If we wish to include extensions in the self-signed certificate we could use a configuration file just like we did for the CSR but we would use x509_extensions instead of req_extensions

Signing:

openssl x509 -req -in testing_1.org.csr -CA testing_1.crt -CAkey testing_1.key -CAcreateserial -out testing_1.org.crt

 

Each issued certificate must contain a unique serial number assigned by the CA. It must be unique for each certificate given by a given CA. OpenSSL keeps the used serial numbers on a file, by default it has the same name as the CA certificate file with the extension replace by srl. So a file named testing_1.srl is created:
 
This unique hex will reflected in CA certificate of testing_1.org.crt. 
This command produces the file testing_1.org.crt which we can examine:

openssl x509 -in testing_1.org.crt -noout -text

 


Root CA Creation using above Key
Self-signed CA Creation
Dynamic Key pair generation and followed by CA generation
Using the device unique, as challenge, to generate dynamically generation of public and private key or CA Certification


Week-3 complete summary:
Week-4 complete summary:
Week-5 complete summary:
Week-6 complete summary:
Week-7 complete summary:
Week-8 complete summary:
Week-9 complete summary:
Week-10 complete summary:
Week-11 complete summary:
Week-12 complete summary:
Week-13 complete summary:



